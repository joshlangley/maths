
// A cpp program to take data from a file and estimate it's integral using the
// trapezoidal rule. Currently only works with a constant delta-x of 1.

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

// Given a file path to a valid csv file, return a parsed 2D array from it
vector<vector<double>> parse_from_file(string file_path) {
    ifstream data_file (file_path);
    vector<vector<double>> data;

    string line;
    string xlabel;
    string ylabel;
    string xpart;
    string ypart;
    int commaPos;

    // Populate data table from data_file
    if ( data_file.is_open() ) {
        while ( getline(data_file, line) ) {
            // Get comma position
            commaPos = line.find(',');
            // If no comma found, yell about it.
            if ( commaPos == -1 ) {
                cout << "FATAL ERROR: Invalid CSV file.\n";
                break;
            }

            // Separate the line into x and y parts
            xpart = line.substr(0,commaPos);
            ypart = line.substr(commaPos+1);

            // If there are no numbers in the line, assume a table header
            if ( line.find_first_of("0123456789.") == string::npos) {
                xlabel = xpart;
                ylabel = ypart;
                continue;
            }

            // Append a new data point after converting string numbers to doubles
            data.push_back( { stod(xpart), stod(ypart) } );
        }
    }

    return data;
}

int main() {

    vector<vector<double>> data = parse_from_file("./numbers.txt");

    // Uncomment to print data in array
//     for (int i = 0; i < data.size(); i++) {
//         for (int j = 0; j < data[i].size(); j++) {
//             cout << i << "," << j << ": " << data[i][j] << " ";
//         }
//         cout << "\n";
//     }
//     cout << "\n";


    // Estimate integral of data using compound trapezoidal rule
    // (uniform delta-x only)
        // Area of trapezoid = (b1+b2)/2 * h  (h = delta-x)
        // So the integral ~= A1 + A2 + ... + An
        // Or integral ~= ((b1+b2)/2 * dx) + ((b2+b3)/2 * dx) + ...

    double dx = 1; // Set constant delta-x
    double sum = 0;
    // Accumulate the area of each trapezoid
    for (int i = 0; i < data.size()-1; i++) {
        dx = data[i+1][0] - data[i][0]; // Calculate dx for this trapezoid

        sum += ((data[i][1] + data[i+1][1]) / 2) * dx;
    }

    // Print the result to stdout
    cout << sum << "\n";
}
